var clientesObtenidos;

function gestionClientes(){
  var url = "https://services.odata.org/v4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
//      console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = divTablaClientes;
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var urlBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";


  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {

      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClientes.value[i].ContactName;

      var columnaCompany = document.createElement("td");
      columnaCompany.innerText = JSONClientes.value[i].CompanyName;

      var columnaPais = document.createElement("td");
      var imgBandera = document.createElement("img");

      imgBandera.classList.add("flag");

      if(JSONClientes.value[1].Country == "UK"){
          imgBandera.src = urlBandera + "United-Kingdom.png";
      }else{
          imgBandera.src = urlBandera + JSONClientes.value[i].Country + ".png";
      }
    //  imgBandera.style = "width:26px";

      columnaPais.appendChild(imgBandera);
      //columnaPais.innerText = JSONClientes.value[i].Country;

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCompany);
      nuevaFila.appendChild(columnaPais);

      tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
